class User < ApplicationRecord
  has_many :microposts, dependent: :destroy
  has_many :active_relationships, class_name:   "Relationship", 
                                  foreign_key:  "follower_id",
                                  dependent:    :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships # can omit source
  attr_accessor :remember_token, :activation_token, :reset_token
  before_save :downcase_email
  before_create :create_activation_digest
  
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255}, 
                    format: { with: VALID_EMAIL_REGEX }, 
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  class << self
    
    # Return a new random token.
    def new_token
      SecureRandom.urlsafe_base64
    end
    
    # Returns the BCrypt hash digest of the given string.
    def digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                    BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end
  end
  
  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end
  
  # Returns true if the given token matches the current token digest.
  def authenticated?(attribute, token)
    digest = self.send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  # Returns a user's status feed.
  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids}) 
                    OR user_id = :user_id", user_id: id)
  end
  
  def follow(other_user)
    self.following << other_user
  end
  
  def following?(other_user)
    self.following.include?(other_user)
  end
  
  def forget
    self.remember_token = nil
    self.update_attribute(:remember_digest, nil)
  end
  
  # Set reset digets to nil to prevent new password resets
  def invalidate_reset_digest
    update_attribute(:reset_digest, nil)
  end
  
  def password_reset_expired?
    self.reset_sent_at < 2.hours.ago
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    self.update_attribute(:remember_digest, User.digest(self.remember_token))
  end
  
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  def send_password_reset_email
    create_new_reset_digest
    UserMailer.password_reset(self).deliver_now
    update_attribute(:reset_sent_at, Time.zone.now)
  end
  
  def unfollow(other_user)
    self.following.delete(other_user)
  end
  
  private
  
    def create_activation_digest 
      self.activation_token = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
    
    # Create a new reset token and digest for password resets
    def create_new_reset_digest
      self.reset_token = User.new_token
      update_attribute(:reset_digest, User.digest(reset_token))
    end
    
    def downcase_email
      self.email.downcase!
    end
end