require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:marcos)
  end
  
  test "should get new" do
    get login_path
    assert_response :success
  end
  
  test "should login" do
    log_in_as(@user)
    assert is_logged_in?
  end
  
  test "should logout" do
    log_in_as(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_response :redirect
  end
end
