require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:marcos)
    @unactivated_user = users(:unactivated)
  end
  
  test "should show user if activated" do
    get user_path(@user)
    assert_response :success
    assert_template 'users/show'
  end
  
  test "should not show user if unactivated" do
    get user_path(@unactivated_user)
    assert_redirected_to root_url
  end
end
