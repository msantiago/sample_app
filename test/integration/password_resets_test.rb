require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:marcos)
  end
  
  test "expired reset token" do
    get new_password_reset_path
    post password_resets_path, 
         params: { password_reset: { email: @user.email } }
    user = assigns(:user)
    user.update_attribute(:reset_sent_at, 3.hours.ago)
    
    # Test edit action/view
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_response :redirect
    follow_redirect!
    assert_match "expired", response.body
    
    # Test update action
    patch password_reset_path(user.reset_token), 
          params: { email: user.email, 
                    user: { password: "validPassword", 
                            password_confirmation: "validPassword" } }
    assert_response :redirect
    follow_redirect!
    assert_match "expired", response.body
  end
  
  test "password resets" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    
    # Invalid email
    post password_resets_path, params: { password_reset: { email: "" } }
    assert_not flash.empty?
    assert_template 'password_resets/new'
    
    # Valid email
    post password_resets_path, 
         params: { password_reset: { email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to root_url
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    
    # Password reset form
    # Get user before requesting another webpage
    user = assigns(:user)
    
    # Wrong email in reset link
    get edit_password_reset_path(user.reset_token, email: "")
    assert_redirected_to root_url
    assert_not flash.empty?
    
    # Invactive user
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_url
    assert_not flash.empty?
    user.toggle!(:activated)
    
    # Correct email, wrong reset token in reset link
    get edit_password_reset_path("wrong token", email: user.email)
    assert_redirected_to root_url
    assert_not flash.empty?
    
    # Correct email and token
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit'
    assert_select "input[type=hidden][name=email][value=?]", user.email
    
    # Invalid password and confirmation
    patch password_reset_path(user.reset_token), 
          params: { email: user.email,
                    user: { password: "foobar", 
                            password_confirmation: "helloworld" } }
    assert_template 'password_resets/edit'
    assert_select 'div#error_explanation'
    
    # Empty password
    patch password_reset_path(user.reset_token),
          params: { email: user.email, 
                    user: { password: "",
                            password_confirmation: "" } }
    assert_template 'password_resets/edit'
    assert_select 'div#error_explanation'
    
    # Valid password and password confirmation
    patch password_reset_path(user.reset_token), 
          params: { email: user.email,
                    user: { password: "validPassword",
                            password_confirmatioN: "validPassword" } }
    assert_redirected_to user_path(user)
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.empty?
    assert is_logged_in?
    
    user.reload
    assert_nil user.reset_digest
  end
end
