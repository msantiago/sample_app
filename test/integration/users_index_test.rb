require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:marcos)
    @non_admin = users(:peter)
  end
  
  test "index of activated users with pagination" do
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination', count: 2
    users = assigns(:users)
    users.each do |user|
      assert user.activated?
      assert_select 'img.gravatar'
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @admin
        assert_select 'a[href=?]', user_path(user), text: 'Delete'
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end
  
  test "index as non-admin" do
    log_in_as(@non_admin)
    get users_path
    # User should not be able to delete anything but their session (logout)
    assert_select 'a[href=?]', logout_path
    assert_select 'a[data-method="delete"]', count: 1 
  end
end
