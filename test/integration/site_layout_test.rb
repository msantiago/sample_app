require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:marcos)
  end
  
  test "layout links for logged out users" do
    get root_path
    
    # Header links
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", login_path
    
    # Footer links
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
  end
  
  # Test links not tested yet for logged in users.
  test "layout links for logged in users" do
    log_in_as(@user)
    get root_path
    
    # Header links
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_path(@user)
    assert_select "a[href=?]", logout_path
  end
  
  test "layout links behavior for logged in users" do
    log_in_as(@user)
    
    # "Users" link
    get users_path
    assert_template 'users/index'
    
    # Profile link
    get user_path(@user)
    assert_template 'users/show'
    
    # Settings link
    get edit_user_path(@user)
    assert_template 'users/edit'
    
    # Logout link
    delete logout_path
    follow_redirect!
    assert_template root_path
  end
  
  test "layout links for home page body" do
    get root_path
    assert_template 'static_pages/home'
    
    assert_select "a[href=?]", signup_path
    get signup_path
    assert_template 'users/new'
    assert_select "title", full_title("Sign up")
  end
end