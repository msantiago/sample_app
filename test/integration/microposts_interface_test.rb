require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:marcos)
    @micropost_max_length = 140
  end
  
  test "micropost interface" do
    log_in_as(@user)
    get root_url
    assert_select 'div.pagination'
    assert_select 'ol.microposts'
    assert_select 'input[type=file]'
    # Invalid submissions
    # Blank micropost
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: "" } }
    end
    assert_select 'div#error_explanation'
    # Micropost too long
    assert_no_difference 'Micropost.count' do
      post microposts_path, 
           params: { micropost: { content: "a" * (@micropost_max_length+1) } }
    end
    assert_select 'div#error_explanation'
    # Valid submission
    content = "Lorem ipsum."
    picture = fixture_file_upload('test/fixtures/rails.png', 'image/png')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: { micropost: { content: content, 
                                                   picture: picture } }
    end
    assert assigns(:micropost).picture?
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    # Delete post
    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    # Visit different user (no delete links should exist)
    get user_path(users(:archer))
    assert_select 'a', text: 'delete', count: 0
  end
  
  test "sidebar micropost count" do
    log_in_as(@user)
    get root_url
    assert_match "34 microposts", response.body
    # Zero microposts
    other_user = users(:malory)
    log_in_as(other_user)
    get root_url
    assert_match "0 microposts", response.body
    # One post
    other_user.microposts.create!(content: "A post")
    get root_url
    assert_match "1 micropost", response.body
  end
end
